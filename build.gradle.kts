import java.time.*
import ca.stellardrift.build.sponge
import ca.stellardrift.build.apAnd

plugins {
    id("ca.stellardrift.opinionated") version "2.0"
    id("ca.stellardrift.templating") version "2.0"
    //id("org.spongepowered.event-impl-gen") version "6.0.0"
}

group = "ca.stellardrift"
version = "1.1-SNAPSHOT"
description = "A server-only elevators plugin for Sponge"

repositories {
    jcenter()
    sponge()
}

opinionated {
    gitlab("zml", "LiftPlates")
    gpl3()
    useJUnit5()
}

license {
    header = file("LICENSE_HEADER")
    ext["year"] = LocalDate.now().year
}

/*tasks.genEventImpl.configure {
    outputFactory = "ninja.leaping.liftplates.event.LiftPlatesEventFactory"
    include("ninja/leaping/liftplates/event/")
}*/

val versionSponge: String by project
dependencies {
    apAnd("compileOnly", "org.spongepowered:spongeapi:$versionSponge")
}
