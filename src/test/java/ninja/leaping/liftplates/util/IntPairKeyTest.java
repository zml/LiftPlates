/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates.util;


import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author zml2008
 */
public class IntPairKeyTest {
    // @Test // (This test takes a long time, so only needs to be run if there are issues)
    public void testIntPairKey() {
        for (int a = Integer.MIN_VALUE >> 16; a < Integer.MAX_VALUE >> 16; a += 5) {
            for (int b = Integer.MIN_VALUE >> 16; b < Integer.MAX_VALUE >> 16; b += 7) {
                long key = IntPairKey.key(a, b);
                assertEquals(a, IntPairKey.key1(key));
                assertEquals(b, IntPairKey.key2(key));
            }
        }
    }
}
