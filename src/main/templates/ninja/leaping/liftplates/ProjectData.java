package ninja.leaping.liftplates;

class ProjectData {
    public static final String ARTIFACT_ID = "${project.name}";
    public static final String NAME = "${project.displayName}";
    public static final String VERSION = "${project.version}";
    public static final String DESCRIPTION = "${project.description}";
}
