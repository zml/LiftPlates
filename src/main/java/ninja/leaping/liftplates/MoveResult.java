/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates;

/**
 * The result from a {@link Lift}'s motion.
 */
public class MoveResult {
    public enum Type {
        CONTINUE(false),
        DELAY(true),
        STOP(true),
        BLOCK(false);

        private final boolean override;

        private Type(boolean override) {
            this.override = override;
        }

        public boolean isOverrideable() {
            return override;
        }
    }

    private final Type type;
    private final int amount;

    public MoveResult(Type type) {
        this(type, 0);
    }

    public MoveResult(Type type, int amount) {
        this.type = type;
        this.amount = amount;
    }

    public Type getType() {
        return type;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "MoveResult{" +
                "type=" + type +
                ", amount=" + amount +
                '}';
    }
}
