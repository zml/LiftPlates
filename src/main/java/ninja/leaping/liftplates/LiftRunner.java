/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates;

import ninja.leaping.liftplates.specialblock.SpecialBlock;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.block.PoweredData;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


/**
 * @author zml2008
 */
public class LiftRunner implements Runnable {
    public static final long RUN_FREQUENCY = 5; // ticks
    private final LiftPlatesPlugin plugin;
    private final Map<Lift, LiftState> movingLifts = new HashMap<>();
    private final Set<Location<World>> triggeredPoints = new LinkedHashSet<>();

    public LiftRunner(LiftPlatesPlugin plugin) {
        this.plugin = plugin;
    }

    public void plateTriggered(Location<World> loc) {
        triggeredPoints.add(loc);
    }

    /**
     * Returns whether a lift or any attached lifts are running
     *
     * @param lift The lift to check
     * @return Whether any of the involved lifts are running
     */
    public boolean isLiftRunning(Lift lift) {
        LiftState state = movingLifts.get(lift);
        LiftContents contents = null;
        if (state != null) {
           contents = state.contents;
        }
        if (contents == null) {
            contents = lift.getContents();
        }
        for (Lift testLift : contents.getLifts()) {
            if (movingLifts.containsKey(testLift)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Stop a lift and any attached lifts
     *
     * @param lift The lift to get contents from for stopping
     */
    public void stopLift(Lift lift) {
        LiftState state = movingLifts.get(lift);
        LiftContents contents = null;
        if (state != null) {
            contents = state.contents;
        }
        if (contents == null) {
            contents = lift.getContents();
        }
        for (Lift testLift : contents.getLifts()) {
            movingLifts.remove(testLift);
        }
    }

    private static class LiftState {
        public int delay = 1;
        public boolean specialBlocksTriggered = true;
        public LiftContents contents;
        public boolean playerCaused = false;
    }

    public void run() {
        for (Iterator<Location<World>> i = triggeredPoints.iterator(); i.hasNext();) {
            boolean triggered = false;
            Location<World> point = i.next();
            if (!point.get(Keys.POWERED).orElse(false)) {
                i.remove();
                continue;
            }
            Lift lift = plugin.getLiftManager(point.getExtent()).getLift(point.getBlockPosition());
            if (lift != null) { // Lift plate
                triggered = true;
                if (!movingLifts.containsKey(lift)) {
                    LiftState liftState = new LiftState();
                    movingLifts.put(lift, liftState);
                } else {
                    LiftState liftState = movingLifts.get(lift);
                    if (liftState.delay == -1) {
                        liftState.delay = 1;
                    }
                }
            } else { // Special block controller plate
                lift = plugin.detectLift(point, true);
                if (lift != null) {
                    Location<World> specialLoc = point.getBlockRelative(Direction.DOWN);
                    SpecialBlock block = lift.getSpecialBlock(specialLoc.getBlockType());
                    if (block != null) {
                        block.plateTriggered(lift, specialLoc);
                        triggered = true;
                    }
                }
            }
            if (triggered && LiftUtil.isPressurePlate(point.getBlockType())) {
                point.remove(PoweredData.class);
            }
            i.remove();
        }
        final Set<Lift> toRemove = new HashSet<>();

        for (Iterator<Map.Entry<Lift, LiftState>> i = movingLifts.entrySet().iterator(); i.hasNext();) {
            Map.Entry<Lift, LiftState> entry = i.next();
            if (toRemove.contains(entry.getKey())) {
                i.remove();
                continue;
            }
            LiftState state = entry.getValue();
            if (state.contents == null) {
                state.contents = entry.getKey().getContents();
            } else {
                state.contents.update();
            }

            boolean hasPlayers = false;
            for (Entity entity : state.contents.getEntities()) {
                if (entity instanceof Player) {
                    hasPlayers = true;
                    break;
                }
            }

            if (!hasPlayers && state.playerCaused) {
                i.remove();
                continue;
            } else {
                state.playerCaused = hasPlayers;
            }

            if (state.delay > 0 && --state.delay == 0) {
                boolean removing = false;
                for (Lift lift : state.contents.getLifts()) {
                    if (lift != entry.getKey() && movingLifts.containsKey(lift)) {
                        toRemove.add(lift);
                        removing = true;
                    }
                }

                if (removing) {
                    i.remove();
                    continue;
                }

                MoveResult result = state.contents.move(!state.specialBlocksTriggered);
                switch (result.getType()) {
                    case DELAY:
                        state.specialBlocksTriggered = false;
                        state.delay += result.getAmount();
                        break;
                    case CONTINUE:
                        state.delay = 1;
                        state.specialBlocksTriggered = true;
                        break;
                    case STOP:
                        state.contents = null;
                        state.delay = -1;
                        state.specialBlocksTriggered = false;
                        break;
                    case BLOCK:
                        i.remove();
                        break;
                }
            }
        }

        for (Lift lift : toRemove) {
            movingLifts.remove(lift);
        }
    }
}
