/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates;

import com.flowpowered.math.vector.Vector3i;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.gson.GsonConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zml2008
 */
public class LiftManager {
    private final LiftPlatesPlugin plugin;
    private final World world;
    private ConfigurationNode store;
    private final ConfigurationLoader<ConfigurationNode> loader;

    /**
     * The lifts stored in this LiftManager
     */
    private final Map<Vector3i, Lift> lifts = new ConcurrentHashMap<>();

    public LiftManager(LiftPlatesPlugin plugin, World world) {
        this.plugin = plugin;
        this.world = world;
        loader = GsonConfigurationLoader.builder().setPath(plugin.getConfigurationFolder().resolve("lifts-" + world.getUniqueId() + ".json")).build();
    }

    public Lift getLift(Vector3i point) {
        return lifts.get(point);
    }

    public Lift getLift(Location<World> loc) {
        if (!world.equals(loc.getExtent())) {
            throw new IllegalArgumentException("Location with mismatched world provided to world-specific LiftManager");
        }
        return lifts.get(loc.getBlockPosition());
    }

    public Lift getOrAddLift(Vector3i point) {
        Lift lift = lifts.get(point);
        if (lift == null && canPlaceLift(point)) {
            lift = new Lift(point);
            lift.setManager(this);
            lifts.put(point, lift);
            save();
        }
        return lift;
    }

    public boolean removeLift(Vector3i point) {
        return lifts.remove(point) != null;
    }

    void updateLiftLocations() {
        Set<Lift> mismatchedLocations = new HashSet<Lift>();
        for (Iterator<Map.Entry<Vector3i, Lift>> i = lifts.entrySet().iterator(); i.hasNext();) {
            Map.Entry<Vector3i, Lift> entry = i.next();
            if (!entry.getKey().equals(entry.getValue().getPosition())) {
                mismatchedLocations.add(entry.getValue());
                i.remove();
            }
        }

        for (Lift lift : mismatchedLocations) {
            lifts.put(lift.getPosition(), lift);
        }
    }

    public boolean canPlaceLift(Vector3i point) {
        return LiftUtil.isPressurePlate(world.getBlockType(point));
    }

    public Collection<Lift> getLifts() {
        return lifts.values();
    }

    public World getWorld() {
        return world;
    }

    public LiftPlatesPlugin getPlugin() {
        return plugin;
    }

    public void load() {
        try {
            store = loader.load();
        } catch (IOException e) {
            plugin.getLogger().warn("Error while loading lifts configuration: " + e.getMessage(), e);
            return;
        }
        ConfigurationNode objects = store.getNode("lifts");
        if (objects.isVirtual()) {
            plugin.getLogger().warn("No 'lifts' list in the configuration!");
            return;
        }

        for (ConfigurationNode child : objects.getChildrenList()) {
            Lift lift;
            try {
                lift = Lift.MAPPER.bindToNew().populate(child);
            } catch (ObjectMappingException e) {
                plugin.getLogger().warn("Unable to load lift at " + Arrays.toString(child.getPath()) + " in world " + getWorld().getName(), e);
                continue;
            }
            if (!canPlaceLift(lift.getPosition())) { // The lift block has been removed since the last load
                plugin.getLogger().warn("Cannot load lift at " + lift.getPosition() + "! Did the world change?");
                continue;
            }
            lift.setManager(this);
            lifts.put(lift.getPosition(), lift);
        }
    }

    public void save() {
        ConfigurationNode lifts = store.getNode("lifts");
        lifts.setValue(null);
        for (Lift lift : this.lifts.values()) {
            try {
                Lift.MAPPER.bind(lift).serialize(lifts.getAppendedNode());
            } catch (ObjectMappingException e) {
                plugin.getLogger().warn("Unable to serializer lift at position " + lift.getPosition() + ", it may have to be recreated!");
            }
        }

        try {
            loader.save(store);
        } catch (IOException e) {
            plugin.getLogger().warn("Error while saving lifts configuration", e);
        }
    }
}
