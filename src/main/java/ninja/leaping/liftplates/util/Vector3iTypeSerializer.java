/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates.util;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

/**
 * A TypeSerializer for {@link Vector3i} instances
 */
public class Vector3iTypeSerializer implements TypeSerializer<Vector3i> {

    @Override
    public Vector3i deserialize(TypeToken<?> typeToken, ConfigurationNode configurationNode) throws ObjectMappingException {
        int x = configurationNode.getNode("x").getInt();
        int y = configurationNode.getNode("y").getInt();
        int z = configurationNode.getNode("z").getInt();
        return new Vector3i(x, y, z);
    }

    @Override
    public void serialize(TypeToken<?> typeToken, Vector3i vec, ConfigurationNode configurationNode) throws ObjectMappingException {
        configurationNode.getNode("x").setValue(vec.getX());
        configurationNode.getNode("y").setValue(vec.getY());
        configurationNode.getNode("z").setValue(vec.getZ());
    }
}
