/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates.util;

import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.world.BlockChangeFlags;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple queue to work around MC physics issues when setting blocks
 */
public class BlockQueue {

    public enum BlockOrder {

        /**
         * Applies changes from the bottom up. This is most useful when filling an area.
         */
        BOTTOM_UP(false, (a, b) -> a.getY() - b.getY()),
        /**
         * Applies changes from the top down. This is most useful when clearing an area.
         */
        TOP_DOWN(true, (a, b) -> {
            return BOTTOM_UP.getComparator().compare(b, a);
        });
        private final Comparator<Vector3i> comparator;
        private final boolean changeLastFirst;

        BlockOrder(boolean changeLastFirst, Comparator<Vector3i> comparator) {
            this.changeLastFirst = changeLastFirst;
            this.comparator = comparator;
        }

        public Comparator<Vector3i> getComparator() {
            return comparator;
        }

        public boolean shouldChangeLastFirst() {
            return changeLastFirst;
        }
    }

    private final BlockOrder order;
    private final World world;
    private final Map<Vector3i, BlockSnapshot> changesNormal = new HashMap<>();
    private final Map<Vector3i, BlockSnapshot> changesLast = new HashMap<>();

    public BlockQueue(World world, BlockOrder order) {
        this.world = world;
        this.order = order;
    }

    public void set(Vector3i point, BlockSnapshot mat) { // TODO: Work with block snapshots?
        BlockState testMat = mat.getState();
        if (testMat.getType() == BlockTypes.AIR) {
            testMat = world.getBlock(point);
        }

        if (needsSideAttachment(testMat)) {
            changesLast.put(point, mat);
            changesNormal.remove(point);
        } else {
            changesNormal.put(point, mat);
            changesLast.remove(point);
        }
    }

    public void setAll(Map<Vector3i, BlockSnapshot> changes) {
        for (Map.Entry<Vector3i, BlockSnapshot> entry : changes.entrySet()) {
            set(entry.getKey(), entry.getValue());
        }
    }

    public void apply() {
        List<Map.Entry<Vector3i, BlockSnapshot>> changesNormalList = new ArrayList<Map.Entry<Vector3i, BlockSnapshot>>(changesNormal.entrySet());
        changesNormalList.sort((a, b) -> order.getComparator().compare(a.getKey(), b.getKey()));
        Iterable<Map.Entry<Vector3i, BlockSnapshot>> first = order.shouldChangeLastFirst() ? changesLast.entrySet() : changesNormalList;
        Iterable<Map.Entry<Vector3i, BlockSnapshot>> second = order.shouldChangeLastFirst() ? changesNormalList : changesLast.entrySet();


        for (Map.Entry<Vector3i, BlockSnapshot> entry : first) {
            applyBlockChange(entry.getKey(), entry.getValue());
        }

        for (Map.Entry<Vector3i, BlockSnapshot> entry : second) {
            applyBlockChange(entry.getKey(), entry.getValue());
        }
    }

    protected void applyBlockChange(Vector3i pt, BlockSnapshot mat) {
        BlockState orig = mat.getState();
        BlockState modified = modifyBlockState(orig);
        if (orig != modified) {
            mat = mat.withState(modified);
        }
        world.restoreSnapshot(pt, mat, true, setNoPhysics(modified.getType()) ? BlockChangeFlags.NONE : BlockChangeFlags.NEIGHBOR_PHYSICS);
    }

    protected BlockState modifyBlockState(BlockState input) {
        return input;
    }

    /**
     * Returns whether the provided block requires an attachment to the side of another block
     * @param data The material to check
     * @return Whether an attachment is required
     */
    private static boolean needsSideAttachment(BlockState data) {
        return data.supports(Keys.ATTACHED)
                || data.getType() == BlockTypes.PISTON;
    }

    /**
     * Returns whether physics should not be run at all when setting a block.
     * This is only used for REALLY weird blocks.
     *
     * @param mat The material to check
     * @return Whether physics should be stopped when setting a block of this type
     */
    private static boolean setNoPhysics(BlockType mat) {
        return mat == BlockTypes.PISTON_EXTENSION
                || mat == BlockTypes.PISTON_HEAD;
    }
}
