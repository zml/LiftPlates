/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates.util;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import ninja.leaping.liftplates.specialblock.SpecialBlock;

/**
 * Type serializer for SpecialBlock instances
 */
public class SpecialBlockTypeSerializer implements TypeSerializer<SpecialBlock> {
    @Override
    public SpecialBlock deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
        return SpecialBlock.byName(value.getString());
    }

    @Override
    public void serialize(TypeToken<?> type, SpecialBlock obj, ConfigurationNode value) throws ObjectMappingException {
        value.setValue(obj.getName());
    }
}
