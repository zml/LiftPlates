/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.base.Preconditions;
import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializers;
import ninja.leaping.liftplates.specialblock.SpecialBlock;
import ninja.leaping.liftplates.util.SpecialBlockTypeSerializer;
import ninja.leaping.liftplates.util.Vector3iTypeSerializer;
import org.slf4j.Logger;
import org.spongepowered.api.Game;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.pagination.PaginationService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.storage.WorldProperties;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static ninja.leaping.liftplates.util.MessageFormatting.normal;
import static org.spongepowered.api.command.args.GenericArguments.enumValue;
import static org.spongepowered.api.command.args.GenericArguments.location;
import static org.spongepowered.api.command.args.GenericArguments.world;

/**
 * @author zml2008
 */
@Plugin(id = ProjectData.ARTIFACT_ID, name = ProjectData.NAME, version = ProjectData.VERSION, description = ProjectData.DESCRIPTION)
public class LiftPlatesPlugin {
    static {
        TypeSerializers.getDefaultSerializers().registerType(TypeToken.of(SpecialBlock.class), new SpecialBlockTypeSerializer());
        TypeSerializers.getDefaultSerializers().registerType(TypeToken.of(Vector3i.class), new Vector3iTypeSerializer());
    }
    @Inject private Game game;
    @Inject private Logger logger;
    @Inject private LiftPlatesConfig config;
    private LiftRunner liftRunner;
    private final Map<UUID, LiftManager> liftManagers = new HashMap<>();

    @Listener
    public void onEnable(GamePreInitializationEvent event) {
        /*ItemStack specialToolStack = game.getRegistry().getItemBuilder()
                .itemType(ItemTypes.BLAZE_ROD)
                .itemData(game.getRegistry().getManipulatorRegistry().getBuilder(DisplayNameData.class).get().create().setCustomNameVisible(true)
                        .setDisplayName(Texts.of(TextColors.GOLD, "Lift Creator")))
                .itemData(game.getRegistry().getManipulatorRegistry().getBuilder(LoreData.class).get().create().add(Texts.of(TextColors.GRAY,
                        "Left-click to create an up lift\n"
                        + "Right-click to create a down lift")))
        .build();*/

        game.getEventManager().registerListeners(this, new LiftPlatesListener(this));

        game.getCommandManager().register(this, CommandSpec.builder()
                .child(CommandSpec.builder()
                        .permission("liftplates.version")
                        .executor((src, args) -> {
                            src.sendMessage(normal(Text.of(ProjectData.NAME, " version ", ProjectData.VERSION)));
                            return CommandResult.success();
                        }).build(), "version", "v")
                .child(CommandSpec.builder()
                        .permission("liftplates.reload")
                        .executor((src, args) -> {
                            try {
                                reload();
                                src.sendMessage(normal(Text.of(ProjectData.NAME, " successfully reloaded")));
                            } catch (Throwable t) {
                                logger.error("Error while reloading (executed by" + src.getName() + ")", t);
                                throw new CommandException(Text.of("Error while reloading " + ProjectData.NAME + ": " + t.getMessage(),
                                        Text.of(TextColors.DARK_RED, "See console for more details")));
                            }
                            return CommandResult.success();
                        }).build(), "reload", "rel", "r")
                .build(), "liftplates", "liftpl", "lp");

        game.getCommandManager().register(this, CommandSpec.builder()
        .permission("liftplates.lift.new.command")
                .description(Text.of("Create a new lift"))
                .arguments(location(Text.of("location")), enumValue(Text.of("direction"), Direction.class))
                .executor((src, args) -> {
                    Location<World> loc = args.<Location<World>>getOne("location").get();
                    Direction direction = args.<Direction>getOne("direction").get();
                    Lift lift = getLiftManager(loc.getExtent()).getOrAddLift(loc.getBlockPosition());
                    if (lift == null) {
                        throw new CommandException(Text.of("No pressure plate at the specified location!"));
                    }

                    lift.setDirection(direction);

                    src.sendMessage(normal(Text.of("Lift successfully created!")));
                    return CommandResult.success();
                })
        .build(), "mklift", "addlift", "createlift");

        game.getCommandManager().register(this, CommandSpec.builder()
        .permission("liftplates.lift.exists")
        .description(Text.of("Shows whether a certain point has a lift"))
        .arguments(location(Text.of("location")))
        .executor((src, args) -> {
            Location<World> testLoc = args.<Location<World>>getOne("location").get();
            Lift lift = getLiftManager(testLoc.getExtent()).getLift(testLoc.getBlockPosition());
            if (lift == null) {
                throw new CommandException(Text.of("There is no lift at the specified location!"));
            } else {
                src.sendMessage(normal(Text.of("There is a lift at " + lift.getPosition() + " moving " + lift.getDirection())));
            }
            return CommandResult.success();
        }).build(), "islift", "liftexists", "haslift");

        game.getCommandManager().register(this, CommandSpec.builder()
                .permission("liftplates.lift.list")
                .description(Text.of("List the lifts in <world>"))
                .arguments(world(Text.of("world")))
                .executor((src, args) -> {
                    WorldProperties world = args.<WorldProperties>getOne("world").get();
                    Optional<World> actualWorld = game.getServer().getWorld(world.getUniqueId());
                    if (!actualWorld.isPresent()) {
                        throw new CommandException(Text.of("World was not loaded!"));
                    }
                    LiftManager manager = getLiftManager(actualWorld.get());
                    game.getServiceManager().provideUnchecked(PaginationService.class).builder()
                            .title(normal(Text.builder("Lifts in world '").append(Text.of(world.getWorldName()), Text.of('\''))))
                            .contents(manager.getLifts().stream().map(input -> normal(Text.builder("Lift at ").append(Text.of(input.getPosition(), " moving ",
                                    input.getDirection())))).collect(Collectors.toList()))
                            .sendTo(src);
                    return CommandResult.success();
                })
                .build(), "lslifts");

        liftRunner = new LiftRunner(this);
        game.getScheduler().createTaskBuilder()
                .execute(liftRunner)
                .delayTicks(LiftRunner.RUN_FREQUENCY)
                .intervalTicks(LiftRunner.RUN_FREQUENCY)
                .submit(this);
    }

    @Listener
    public void onServerStarting(GameStartedServerEvent event) {
        SpecialBlock.registerDefaults();
        try {
            config.load();
        } catch (IOException e) {
            this.logger.error("Unable to load LiftPlates configuration! This is fatal!", e);
            this.game.getServer().shutdown();
        }

    }


    @Listener
    public void onStopping(GameStartedServerEvent event) {
        for (LiftManager manager : liftManagers.values()) {
            manager.save();
        }
    }

    public void reload() {
        try {
            this.config.load();
        } catch (IOException e) {
            this.logger.error("Unable to load configuration!", e);
        }
        for (LiftManager manager : liftManagers.values()) {
            manager.load();
        }
    }

    public LiftRunner getLiftRunner() {
        return liftRunner;
    }

    public LiftPlatesConfig getConfiguration() {
        return config;
    }

    public LiftManager getLiftManager(World world) {
        Preconditions.checkNotNull(world);
        LiftManager manager = liftManagers.get(world.getUniqueId());
        if (manager == null) {
            manager = new LiftManager(this, world);
            manager.load();
            liftManagers.put(world.getUniqueId(), manager);
        }
        return manager;
    }

    public Lift detectLift(Location<World> loc, boolean nearby) {
        Preconditions.checkNotNull(loc, "loc");
        Vector3i pointLoc = loc.getBlockPosition();
        Lift lift = getLiftManager(loc.getExtent()).getLift(pointLoc);
        if (lift == null && nearby) {
            return detectLift(loc, (config.maxLiftSize + 1) * (config.maxLiftSize + 1), pointLoc, new ArrayList<Vector3i>(10));
        }
        return lift;
    }


    private Lift detectLift(Location<World> orig, int maxDistSq, Vector3i loc, List<Vector3i> visited) {
        if (orig.getBlockPosition().distanceSquared(loc) > maxDistSq) {
            return null;
        }
        LiftManager manager = getLiftManager(orig.getExtent());
        Lift lift = manager.getLift(loc);
        if (lift != null) {
            return lift;
        }

        boolean lastChance = false;
        int maxCount = orig.getExtent().getBlockMax().getY() - loc.getY();
        for (int i = 0; i < maxCount; ++i) {
            Vector3i raisedPos = loc.add(0, i, 0);
            lift = manager.getLift(raisedPos);
            if (lift != null) {
                return lift;
            } else {
                if (orig.getExtent().getBlockType(raisedPos) != BlockTypes.AIR) {
                    if (lastChance) {
                        break;
                    } else {
                        lastChance = true;
                    }
                }
            }
        }

        for (int i = 0; i <= loc.getY(); ++i) {
            Vector3i loweredY = loc.sub(0, i, 0);
            lift = manager.getLift(loweredY);
            if (lift != null) {
                return lift;
            } else {
                if (orig.getExtent().getBlockType(loweredY) != BlockTypes.AIR) {
                    break;
                }
            }
        }

        visited.add(loc);
        for (Direction face : LiftUtil.NSEW_FACES) {
            Vector3i newLoc = loc.add(face.asBlockOffset());
            if (visited.contains(newLoc)) {
                continue;
            }

            lift = detectLift(orig, maxDistSq, newLoc, visited);
            if (lift != null) {
                return lift;
            }
        }
        return null;
    }

    public Game getGame() {
        return game;
    }

    public Logger getLogger() {
        return logger;
    }

    public Path getConfigurationFolder() {
        return config.getConfigDir();
    }
}
