/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates;

import com.flowpowered.math.vector.Vector3i;
import ninja.leaping.liftplates.event.SpecialBlockRegisterEvent;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.block.NotifyNeighborBlockEvent;
import org.spongepowered.api.event.filter.data.Has;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains the event listeners. These mostly call other bits of the code.
 * @author zml2008
 */
public class LiftPlatesListener {
    public static final Pattern LIFT_SIGN_PATTERN = Pattern.compile("\\[lift:([^]]+)]");
    private final LiftPlatesPlugin plugin;

    public LiftPlatesListener(LiftPlatesPlugin plugin) {
        this.plugin = plugin;
    }

    @Listener
    public void onPressPlate(InteractBlockEvent.Secondary event, @Has(SignData.class) BlockSnapshot targetBlock) {
            Optional<List<Text>> lines = targetBlock.get(Keys.SIGN_LINES);
            if (lines.isPresent()) {
                Matcher match = LIFT_SIGN_PATTERN.matcher(lines.get().get(0).toPlain());
                if (match.matches()) {
                    final String action = match.group(1);
                    // TODO: Handle lift signs if I still want to use them
                }
            }
    }

    @Listener(order = Order.LAST)
    public void onBlockBreak(ChangeBlockEvent.Break event) {
        for (Transaction<BlockSnapshot> trans : event.getTransactions()) {
            BlockSnapshot block = trans.getOriginal();
            Location<World> pos = block.getLocation().orElse(null);
            if (pos == null) {
                plugin.getLogger().warn("Block broken with null location at {}", block.getPosition());
                continue;
            }
            if (plugin.getLiftManager(pos.getExtent()).getLift(pos) != null) {
                plugin.getLiftManager(pos.getExtent()).removeLift(pos.getBlockPosition());
            } else {
                final Vector3i above = block.getPosition().add(Direction.UP.asBlockOffset());
                plugin.getGame().getScheduler().createTaskBuilder().delayTicks(1L).execute(() -> {
                    if (pos.getExtent().getBlockType(above) == BlockTypes.AIR &&
                            plugin.getLiftManager(pos.getExtent()).getLift(above) != null) {
                        plugin.getLiftManager(pos.getExtent()).removeLift(above);
                    }
                }).submit(plugin);
            }
        }
    }

    @Listener
    public void onSpecialBlockRegister(SpecialBlockRegisterEvent event) {
        LiftPlatesConfig config = plugin.getConfiguration();
        if (!config.specialBlocks.containsValue(event.getRegisteredBlock())) {
            config.specialBlocks.put(event.getRegisteredBlock().getDefaultType(), event.getRegisteredBlock());
            try {
                config.save();
            } catch (IOException e) {
                // Ignore, oh well
            }
        }
    }

    @Listener
    public void onPlateToggle(NotifyNeighborBlockEvent event) {
        plugin.getLogger().info("Received notify neighbor event");
        plugin.getLogger().info("Cause stack: " + event.getCause());
        plugin.getLogger().info("Source: " + event.getSource());
        /*
        if (LiftUtil.isPressurePlate(event.getBlock().getBlockType())) {
            if (event.getNewSignalStrength() > 0 && event.getOldSignalStrength() == 0) { // Turning on
                plugin.getLiftRunner().plateTriggered(event.getBlock());
            }
        }*/
    }
}
