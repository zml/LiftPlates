/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates.specialblock;

import com.flowpowered.math.vector.Vector3i;
import ninja.leaping.liftplates.Lift;
import ninja.leaping.liftplates.LiftContents;
import ninja.leaping.liftplates.LiftRunner;
import ninja.leaping.liftplates.MoveResult;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.HashSet;
import java.util.Set;

/**
 * This special block stops the lift util its associated pressure plate is re-triggered
 */
public class StationSpecialBlock extends SpecialBlock {
    private final Set<Location<World>> activeBlocks = new HashSet<>();
    public StationSpecialBlock() {
        super("Station", BlockTypes.GOLD_BLOCK);
    }

    protected StationSpecialBlock(String name, BlockType type) {
        super(name, type);
    }

    @Override
    public MoveResult liftActed(Lift lift, LiftContents contents) {
        return new MoveResult(MoveResult.Type.STOP);
    }

    @Override
    public void plateTriggered(Lift lift, Location<World> block) {
        if (!activeBlocks.contains(block)) {
            activeBlocks.add(block);
            CallLift call = new CallLift(block, lift);
            call.task = lift.getPlugin().getGame().getScheduler().createTaskBuilder()
                    .execute(call)
                    .intervalTicks(LiftRunner.RUN_FREQUENCY)
                    .submit(lift.getPlugin());
        }
    }

    private class CallLift implements Runnable {
        private Task task;
        private final Direction direction;
        private final Location<World> target;
        private final Lift lift;
        private Vector3i nearestLiftBlock;

        public CallLift(Location<World> target, Lift lift) {
            this.target = target;
            this.lift = lift;
            LiftContents contents = lift.getContents();

            Vector3i blockLoc = target.getBlockPosition();
            final int requiredY = lift.getPosition().getY() - 1;
            Vector3i nearestLoc = null;
            int distance = Integer.MAX_VALUE;

            for (Vector3i loc : contents.getBlocks()) {
                if (loc.getY() == requiredY) {
                    if (loc.distanceSquared(blockLoc) < distance) {
                        nearestLoc = loc;
                    }
                }
            }

            if (nearestLoc == null) {
                throw new IllegalStateException("No nearest location found from the lift!");
            }
            this.nearestLiftBlock = nearestLoc;

            Direction liftDirection = lift.getDirection();

            // Calculate the distances to travel and restrict them to directions the lift can move in normal operation
            Vector3i delta = blockLoc.sub(nearestLoc).mul(liftDirection.asBlockOffset().abs());
            delta.div(delta.getX() == 0 ? 1 : Math.abs(delta.getX()),
                    delta.getY() == 0 ? 1 : Math.abs(delta.getY()),
                    delta.getZ() == 0 ? 1 : Math.abs(delta.getZ()));

            Direction moveFace = Direction.getClosest(delta.toDouble()); // TODO: Does this get what I want?
            /*for (Direction face : Direction.values()) {
                if (face.toVector3d().toInt().equals(delta)) {
                    moveFace = face;
                    break;
                }
            }*/


            if (moveFace == null) {
                throw new IllegalArgumentException("No BlockFace for direction that lift is supposed to move (" + delta + "!");
            } else if (moveFace == Direction.NONE) {

            }
            direction = moveFace;
        }

        public void run() {
            lift.getPlugin().getLiftRunner().stopLift(lift);
            final Vector3i blockLoc = target.getBlockPosition();
            LiftContents contents = lift.getContents();
            final int requiredY = lift.getPosition().getY() - 1;
            Vector3i nearestLoc = null;
            int distance = Integer.MAX_VALUE;

            for (Vector3i loc : contents.getBlocks()) {
                if (loc.getY() == requiredY) {
                    if (loc.distanceSquared(blockLoc) < distance) {
                        nearestLoc = loc;
                    }
                }
            }

            if (nearestLoc == null) {
                throw new IllegalStateException("No nearest location found from the lift!");
            }

            this.nearestLiftBlock = nearestLoc;
            final Vector3i delta = nearestLiftBlock.sub(blockLoc).mul(direction.asBlockOffset());

            contents.update();
            MoveResult result = delta.lengthSquared() == 0 ? new MoveResult(MoveResult.Type.STOP) :
                    contents.move(direction, true);
            if ((result.getType() == MoveResult.Type.STOP
                    || result.getType() == MoveResult.Type.BLOCK) && this.task != null) {
                task.cancel();
                this.task = null;
                activeBlocks.remove(target);
            }
        }
    }
}
