/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates.specialblock;

import ninja.leaping.liftplates.Lift;
import ninja.leaping.liftplates.LiftContents;
import ninja.leaping.liftplates.MoveResult;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.util.Direction;

/**
 * This special block prevents the lift from moving farther in the given direction
 */
public class StopSpecialBlock extends StationSpecialBlock {
    private final Direction direction;
    public StopSpecialBlock(Direction direction, BlockType type) {
        super("Stop" + direction, type);
        this.direction = direction;
    }

    @Override
    public MoveResult liftActed(Lift lift, LiftContents contents) {
        if (lift.getDirection() == direction) {
            return new MoveResult(MoveResult.Type.BLOCK);
        } else {
            return new MoveResult(MoveResult.Type.CONTINUE);
        }
    }
}
