/*
 * LiftPlates -- simple elevators
 * Copyright (C) 2020 zml
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ninja.leaping.liftplates.specialblock;

import ninja.leaping.liftplates.Lift;
import ninja.leaping.liftplates.LiftContents;
import ninja.leaping.liftplates.MoveResult;
import ninja.leaping.liftplates.LiftRunner;
import org.spongepowered.api.block.BlockType;

/**
 * This special block delays the elevator for the given amount of cycles ({@link LiftRunner#RUN_FREQUENCY} ticks)
 */
public class DelaySpecialBlock extends SpecialBlock {
    private final int cycles;
    public DelaySpecialBlock(BlockType type, int cycles) {
        super("Delay" + cycles, type);
        this.cycles = cycles;
    }

    @Override
    public MoveResult liftActed(Lift lift, LiftContents contents) {
        return new MoveResult(MoveResult.Type.DELAY, cycles);
    }
}
